def sort(array):
    _sort(array, 0, len(array) - 1)

def _sort(array, lo, hi):
    if(lo >= hi):
        return

    lt, gt = lo, hi
    v, i = array[lo], lo

    while i <= gt:
        if(array[i] < v):
            swap(array, i, lt)
            lt = lt + 1
            i = i + 1

        elif(array[i] > v):
            swap(array, i, gt)
            gt = gt - 1
        else:
            i = i + 1

    _sort(array, lo, lt - 1)
    _sort(array, gt + 1, hi)



def less(a, b):
    return a < b

def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp

def main():
    array = ['x', 'c', 'g', 'a', 'c', 't', 'a', 'd', 'k']
    print(array)
    sort(array)
    print(array)

if __name__ == "__main__":
    main()
