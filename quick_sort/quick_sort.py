def sort(array):
    _sort(array, 0, len(array) - 1)

def _sort(array, lo, hi):
    if(lo >= hi):
        return

    j = partition(array, lo, hi)
    _sort(array, lo, j - 1)
    _sort(array, j + 1, hi)

def partition(array, lo, hi):
    i = lo + 1
    j = hi

    while True:

        while less(array[i], array[lo]):
            i = i + 1
            if(i == hi):
                break

        while less(array[lo], array[j]):
            j = j - 1
            if(j == lo):
                break

        if(i >= j):
            break

        swap(array, i, j)
    swap(array, lo, j)
    return j

def less(a, b):
    return a < b

def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp

def main():
    array = [5, 2, 4, 6, 1, 3]
    print(array)
    sort(array)
    print(array)

if __name__ == "__main__":
    main()
