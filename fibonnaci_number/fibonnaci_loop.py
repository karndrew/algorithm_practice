def fibonnaci_number(index):
    number_previous = 0
    number = 1

    if index is 1:
        return number_previous
    if index < 3:
        return number

    i = 3
    while i <= index:
        temp = number
        number = number_previous + number
        number_previous = temp
        i += 1

    return number

def main():
    print("input index of number")
    index = int(input())
    print("fibonnaci number for index {} is {}".format(index, fibonnaci_number(index)))

if __name__ == "__main__":
    main()
