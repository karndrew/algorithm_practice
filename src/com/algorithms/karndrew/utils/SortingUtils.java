package com.algorithms.karndrew.utils;

public class SortingUtils {
    private SortingUtils() {
    }

    public static boolean isSorted(int[] array, int hi) {
        for(int i = 1; i < hi; i++){
            if(less(array[i], array[i - 1])){
                return false;
            }
        }
        return true;
    }

    public static boolean less(int first, int second){
        return Integer.compare(first, second) < 0;
    }
}
