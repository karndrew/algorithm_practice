package com.algorithms.karndrew.two_sum;

/*
 Find out if there is a pair(s) of numbers in an integer array that produces a given sum.
Array can be sorted and unsorted.
Ex. [1, 2, 4, 9] - sum = 8 (no)
 [1, 2, 4, 4] - sum = 8 (yes)
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.algorithms.karndrew.utils.SortingUtils.isSorted;

public class TwoSumProblem {
    /*
        O(n) solution (Works only with sorted arrays).
     */
    public static int[] search(int[] nums, int sum) {
        int lo = 0, hi = nums.length - 1;

        assert isSorted(nums, hi);

        while (lo < hi) {
            int addition = nums[lo] + nums[hi];

            if (addition > sum) {
                hi--;
            } else if (addition < sum) {
                lo++;
            } else {
                return new int[]{nums[lo], nums[hi]};
            }
        }

        return new int[]{};
    }

    /*
      n * log(n) solution (Works only with sorted arrays).
     */
    public static int[] search2(int[] nums, int sum) {
        assert isSorted(nums, nums.length - 1);

        for (int i = 0; i < nums.length; i++) {
            int element = nums[i];
            int diff = sum - element;

            int found = Arrays.binarySearch(nums, diff);
            if (found >= 0 && found != i) {
                return new int[]{element, nums[found]};
            }
        }
        return new int[]{};
    }

    /*
        O(n) solution (Works not only with sorted arrays).
     */
    public static int[] search3(int[] nums, int sum) {
        Set<Integer> additives = new HashSet<>();
        for (int element : nums) {
            int additive = sum - element;
            if (additives.contains(element)) {
                return new int[]{element, additive};
            }

            additives.add(additive);
        }
        return new int[]{};
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(search(new int[]{1, 2, 4, 9}, 8)));
        System.out.println(Arrays.toString(search2(new int[]{1, 2, 4, 4}, 8)));
        System.out.println(Arrays.toString(search3(new int[]{9, 2, 3, 4, -1}, 8)));
    }
}
