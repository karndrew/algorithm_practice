package com.algorithms.karndrew.find_all_words_for_letters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class WordFind {
    private static final int ALPHABET_SIZE = 26;

    private static int[] letterOccurences(char[] letters){
        int[] occurences = new int[ALPHABET_SIZE];

        for (char letter : letters){
            occurences[letter - 'a']++;
        }

        return occurences;
    }

    public static List<String> getWords(List<String> dictionary, char[] letters, int minSize){
        int[] letterOccurences = letterOccurences(letters);

        List<String> words = new ArrayList<>();

        for(String word : dictionary){
            int[] occurences = new int[ALPHABET_SIZE];
            boolean validWord = true;

            for(char letter : word.toCharArray()){
                int index = letter - 'a';
                occurences[index]++;

                if(occurences[index] > letterOccurences[index]){
                    validWord = false;
                    break;
                }
            }

            if(validWord && word.length() >= minSize){
                words.add(word);
            }
        }
        return words;
    }

    public static void main(String[] args) {
        List<String> dictionary = new ArrayList<>();
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(new File("dictionary.txt"));

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;

            while ((line = reader.readLine()) != null) {
                try {
                    dictionary.add(line.replaceAll("[^a-zA-Z]", "").toLowerCase());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        char[] letters = {'p', 'c', 'b', 'q', 'i', 't', 'r', 'a', 'o', 'c', 'e', 'n'};

        System.out.println(getWords(dictionary, letters, 7));
    }
}
