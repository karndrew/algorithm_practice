package com.algorithms.karndrew.power_of_two;

/*
Дано число A. Написати програму, яка буде виводити слово YES, якщо це число є якоюсь степінню двійки, в противному
випадку вивести NO. (32 є степінню(5) двійки "YES", 98 не є степінню двійки "NO").

 */
public class PowerOftwo {
    public static boolean is(int number) {
        return (number != 0) && ((number & (number - 1)) == 0);
    }

    public static boolean is2(int number) {
        if (number == 0) {
            return false;
        }

        if (number != 1) {
            if (number % 2 != 0) {
                return false;
            }
            return is2(number / 2);
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(is(32));
        System.out.println(is(0));
        System.out.println(is(1));
        System.out.println(is(33));
        System.out.println(is(64));

        System.out.println();

        System.out.println(is2(32));
        System.out.println(is2(0));
        System.out.println(is2(1));
        System.out.println(is2(33));
        System.out.println(is2(64));
    }
}
