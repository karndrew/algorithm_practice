package com.algorithms.karndrew.power;

/*
    Implement power operation efficiently.
 */

public class Power {
    public static int doIt(int number, int power){
        if(power == 0){
            return 1;
        }

        int pow = doIt(number, power / 2);

        if((power & 1) == 1){
            return number * pow * pow;
        }

        return pow * pow;
    }

    public static void main(String[] args) {
        System.out.println(doIt(2, 4));
    }
}
