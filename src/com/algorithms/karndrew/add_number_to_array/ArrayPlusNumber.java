package com.algorithms.karndrew.add_number_to_array;

import java.util.Arrays;

public class ArrayPlusNumber {
    public static int[] convert(int[] number) {
        int[] reserve = new int[number.length + 1];
        int carry = 1;

        for (int i = number.length - 1; i >= 0; i--) {
            int addition = number[i] + carry;

            if (addition == 10) {
                carry = 1;
            }
            else {
                carry = 0;
            }

            number[i] = addition % 10;
            reserve[i] = addition % 10;
        }

        if(carry == 1){
            reserve[0] = carry;
            return reserve;
        }

        return number;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(convert(new int[]{1, 9, 9})));
    }
}
