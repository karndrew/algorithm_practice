#If we list all the natural numbers below 10 that are multiples of 3 or 5, we
#get 3, 5, 6 and 9. The sum of these multiples is 23.

#Find the sum of all the multiples of 3 or 5 below 1000.
def multiples_sum():
    limit = 1000
    i = 0
    sum = 0
    while i < limit:
        if (i % 3 is 0) or (i % 5 is 0):
            sum += i
        i += 1

    return sum

def main():
    print("sum of all the multiples of 3 or 5 below 1000 is {}".format(multiples_sum()))

if __name__ == "__main__":
    main()
