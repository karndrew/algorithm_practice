
target = 1000

def sum_divisible_by(n):
    p = int(target / n)
    return n * int(((p * (p + 1)) / 2)) #n * sum (gauss sum = (n * (n + 1)) / 2
                                        # for nat sequence)

def sum_div():
    return sum_divisible_by(3) + sum_divisible_by(5) - sum_divisible_by(15)


def main():
    print("sum of all the multiples of 3 or 5 below 1000 is {}".format(sum_div()))

if __name__ == "__main__":
    main()
