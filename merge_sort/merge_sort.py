def sort(array):
    aux = [0, 0, 0, 0, 0, 0]
    _sort(array, aux, 0, len(array) - 1)

def _sort(array, aux, lo, hi):
    if lo >= hi:
        return

    mid = int(lo + (hi - lo) / 2)
    _sort(array, aux, lo, mid)
    _sort(array, aux, mid + 1, hi)
    merge(array, aux, lo, mid, hi)

def merge(array, aux, lo, mid, hi):
    for k in range(lo, hi + 1):
        aux[k] = array[k]

    i, j = lo, mid + 1

    for k in range(lo, hi + 1):
        if i > mid:
            array[k] = aux[j]
            j = j + 1
        elif j > hi:
            array[k] = aux[i]
            i = i + 1
        elif less(aux[j], aux[i]):
            array[k] = aux[j]
            j = j + 1
        else:
            array[k] = aux[i]
            i = i + 1

def less(a, b):
    return a < b

def main():
    array = [2, 6, 3, 1, 5, 4]
    sort(array)
    print(array)

if __name__ == "__main__":
    main()
