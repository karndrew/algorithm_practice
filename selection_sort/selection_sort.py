def sort(array):
    size = len(array)
    for i in range(0, size):
        min = i

        for j in range(i + 1, size):
            if(less(array[j], array[min])):
                min = j

        if(min is not i):
            swap(array, i, min)

def less(a, b):
    return a < b

def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp

def main():
    array = [5, 2, 4, 6, 1, 3]
    print(array)
    sort(array)
    print(array)

if __name__ == "__main__":
    main()
