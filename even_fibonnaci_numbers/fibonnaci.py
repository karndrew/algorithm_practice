def fibonnaci_number(number):
    if number is 0:
        return 0
    if number < 3:
        return 1
    return fibonnaci_number(number - 1) + fibonnaci_number(number - 2)

def main():
    print("input index of number")
    number = int(input())
    print("fibonnaci number for index {} is {}".format(number, fibonnaci_number(number)))

if __name__ == "__main__":
    main()
