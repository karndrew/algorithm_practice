def sort(array):
    length = len(array)
    if length > 0:
        for i in range(1, length):
            element = array[i]
            j = i - 1
            while j >= 0 and array[j] > element:
                array[j + 1] = array[j]
                j = j - 1
            array[j + 1] = element


def main():
    array = [5, 2, 4, 6, 1, 3]
    print(array)
    sort(array)
    print(array)

if __name__ == "__main__":
    main()
