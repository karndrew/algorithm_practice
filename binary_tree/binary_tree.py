class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

class Tree:
    def __init__(self):
        self.root = None

    def getRoot(self):
        return self.root

    def add(self, data):
        if(self.root is None):
            self.root = Node(data)
        else:
            self._add(data, self.root)

    def _add(self, data, node):
        if(data < node.data):
            if(node.left is not None):
                self._add(data, node.left)
            else:
                node.left = Node(data)
        elif(data > node.data):
            if(node.right is not None):
                self._add(data, node.right)
            else:
                node.right = Node(data)
        else:
            node.data = data

    def find(self, data):
        if(self.root is not None):
            return self._find(data, self.root)
        else:
            return None

    def _find(self, data, node):
        if(data == node.data):
            return node
        elif(data < node.data and node.left is not None):
            self._find(data, node.left)
        elif(data > node.data and node.right is not None):
            self._find(data, node.right)

    # TODO: Implement deletion for BST (from notes)
    def deleteTree(self):
        # garbage collector will do this for us.
        self.root = None

    def __str__(self):
        if self.root is not None:
            self._printTree(self.root)
        return '.'

    def _printTree(self, node):
        if node is not None:
            self._printTree(node.left)
            print(str(node.data) + ' ')
            self._printTree(node.right)

def main():
    tree = Tree()
    tree.add(3)
    tree.add(4)
    tree.add(0)
    tree.add(8)
    tree.add(2)
    tree.add(3)

    str(tree)
    print(tree.find(2))

if __name__ == "__main__":
    main()
